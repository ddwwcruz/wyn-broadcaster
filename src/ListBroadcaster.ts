import { Broadcaster } from './Broadcaster';

const LIST = Symbol('list');

export class ListBroadcaster<T> extends Broadcaster<[T, T[], ListActionType]> {
  constructor() {
    super();
    this[LIST] = [];
  }

  get value(): T[] {
    return this[LIST];
  }

  get length() {
    return this.value.length;
  }

  /**
   * Pushes a value to the end of the list
   * 
   * @param value New Value
   */
  push(value: T) {
    this.value.push(value);
    this.broadcast([value, this.value, 'PUSH']);
  }

  /**
   * Remove value from end of list
   * 
   * @returns value that has been removed
   */
  pop() {
    let value = this.value.pop();
    this.broadcast([value, this.value, 'POP']);
  }

  /**
   * Removes value from the beginning of the list
   * 
   * @returns value that has been removed
   */
  shift() {
    let value = this.value.shift();
    this.broadcast([value, this.value, 'SHIFT']);
  }

  /**
   * Adds value to the beginning of the list
   * 
   * @param value Value to be added to the list
   */
  unshift(value: T) {
    this.value.unshift(value);
    this.broadcast([value, this.value, 'UNSHIFT']);
  }

  /**
   * Maps the list to an array
   * 
   * @param cb Callback that maps the list
   * @param args Extra arguments to be passed to the callback
   */
  map<U>(cb: MapCb<T, U>, ...args: any[]) {
    return Array.from(map(this.value, cb, ...args));
  }
}

function* map<T, U>(array: T[], cb: MapCb<T, U>, ...args: any[]) {
  for (let [index, value] of array.entries()) {
    yield cb(value, index, ...args);
  }
}

export type ListActionType = 'PUSH' | 'POP' | 'UNSHIFT' | 'SHIFT';

export type MapCb<T, U> = (value: T, index: number, ...args: any[]) => U;
