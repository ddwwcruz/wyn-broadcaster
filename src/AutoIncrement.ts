const VALUE = Symbol('value');

export class AutoIncrement {
  constructor(value: number = 0) {
    this[VALUE] = value;
  }

  get value(): number {
    return ++this[VALUE];
  }
}
