import { AutoIncrement } from './AutoIncrement';

const broadcasterAI = new AutoIncrement();

const ID = Symbol('id');
const AI = Symbol('ai');
const SUBSCRIBERS = Symbol('subscribers');

export class Broadcaster<T> {
  constructor() {
    this[ID] = broadcasterAI.value;
    this[AI] = new AutoIncrement();
    this[SUBSCRIBERS] = new Map<number, Subscriber<T>>();
  }

  get id(): number {
    return this[ID];
  }

  subscribe(subscriber: Subscriber<T>) {
    let id: number = this[AI].value;
    let subscribers: Map<number, Subscriber<T>> = this[SUBSCRIBERS];
    subscribers.set(id, subscriber);

    return id
  }

  unsubscribe(id: number) {
    let subscribers: Map<number, Subscriber<T>> = this[SUBSCRIBERS];
    return subscribers.delete(id);
  }

  broadcast(args: T) {
    let subscribers: Map<number, Subscriber<T>> = this[SUBSCRIBERS];
    for (let [id, func] of subscribers) {
      func(args, id);
    }
  }
}

export type Subscriber<T> = (args: T, id: number) => any;
