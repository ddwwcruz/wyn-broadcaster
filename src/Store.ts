import { Broadcaster } from './Broadcaster';

const VALUE = Symbol('value');

export class Store<T> extends Broadcaster<T> {
  [VALUE]: T;

  constructor(value: T) {
    super();
    this[VALUE] = value;
  }

  get value() {
    return Object.freeze(this[VALUE]);
  }

  set value(args: T) {
    this[VALUE] = args;
    this.broadcast(args);
  }
}
