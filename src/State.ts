import { Store } from './Store';

const ORIGINAL_STATE = Symbol('ORIGINAL_STATE');

export class State<T = any> extends Store<T> {
  [ORIGINAL_STATE]: T;

  constructor(value: T) {
    super(value);
    this[ORIGINAL_STATE] = value;
  }

  set(values: Partial<T>) {
    this.value = Object.assign({}, this.value, values);
  }

  reset() {
    this.set(this[ORIGINAL_STATE]);
  }
}
