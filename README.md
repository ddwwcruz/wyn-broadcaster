# wyn-broadcaster

# Usage

## Actions

Action broadcasters broadcast actions to its subscribers.

```js
const { Action } = require('wyn-broadcaster');

// creating a new action
const someAction = new Action();

// subscribing
const id = someAction.subscribe(function (value, id) {
    // value - Value that has been broadcasted by the broadcaster
    // id - Unique ID of the subscriber (unique only to this broadcaster)
});

// trigger the action
someAction.broadcast('some value');

// unsubscribe from the action
someAction.unsubscribe(id);
```

## Stores
Stores keep their value as they pass it around. Subscribing, unsubscribing is the same as action

```js
const { Store } = require('wyn-broadcaster');

// create a new store
const someStore = new Store('initial value');

// getting the current value of the store
const currentValue = someStore.value;


// new value to store
someStore.value = 'new value'
```

## State

```js
const { State } = require('wyn-broadcaster');
const someState = new State({
    someValue: 'Initial Value',
    otherValue: 'Another Initial Value'
});

// Set the new state (only changes changes what you specify to be changed)
someState.set({
    someValue: 'New Value'
});
```

## List Broadcaster

```js
const { ListBroadcaster } = require('wyn-broadcaster');

const listBroadcaster = new ListBroadcaster();

const id = listBroadcaster.subscribe(([item, list, actionType], id) => {
    // item - Value that has been just removed or just added depending on the action
    // list - Actual list
    // actionType - Type of action done on the list. Can be PUSH, POP, SHIFT, UNSHIFT
    // id - ID of the subscriber
});

// get the length of the list
listBroadcaster.length;

// get the actual list itself
listBroadcaster.value;

// adds an item to the end of the list
listBroadcaster.push('new value');

// removes an item from the end of the list
const poppedValue = listBroadcaster.pop();

// adds an item to the beginning of the list
listBroadcaster.unshift('new value');

// removes an item from the beginning of the list
const shiftedValue = listBroadcaster.shift();

// maps the items of the list to an array
const newArray = listBroadcaster.map(function (value, index, extraArgs) {
    return index; // or any new value
}, extraArgs);

```
