const broadcasters = require('./dist');

if (typeof exports !== 'undefined') {
  exports.Broadcaster = broadcasters.Broadcaster;
  exports.Action = broadcasters.Action;
  exports.Store = broadcasters.Store;
  exports.State = broadcasters.State;
  exports.ListBroadcaster = broadcasters.ListBroadcaster;
}

if (typeof window !== 'undefined') {
  if (typeof define === 'function' && define['amd']) {
    define(() => (broadcasters));
  }
}
